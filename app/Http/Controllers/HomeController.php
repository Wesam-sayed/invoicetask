<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Company;
use App\Models\Currency;
use App\Models\Invoice;
use App\Models\Invoice_item;
use App\Models\Item;
use App\Models\ItemTax;
use Illuminate\Http\Request;
use DB;
use Session;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $company_id = session('company')['id'];
        $branch_id = session('branch');
        $currencies = Currency::get();

        $branch = Branch::find($branch_id);
        return view('invoice.create',compact('branch','branch_id','company_id','currencies'));
    }

    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'invoice_price_type' => 'required|min:0|max:1',
            'total_amount' => 'required|numeric|between:0,999999999999999.99999',
            'company_id' => 'required|int',
            'branch_id' => 'required|int',
            'invoice_discount' => 'required|numeric|between:0,999999999999999.99999',
            'amount_without_tax' => 'required|numeric|between:0,999999999999999.99999',
            'total_tax' => 'required|numeric|between:0,999999999999999.99999',
            'item.*' => 'required',
            'item.*.name' => 'required|string',
            'item.*.id' => 'nullable|int',
            'item.*.sales_kit_id' => 'nullable|int',
            'item.*.quantity' => 'required|numeric|between:0,999999999999999.99999',
            'item.*.unit_price' => 'required|numeric|between:0,999999999999999.99999',
            'item.*.value_difference' => 'required|numeric|between:0,999999999999999.99999',
            'item.*.currency' => 'required|int',
            'item.*.currency_rate' => 'required|numeric|between:0,999999999999999.99999',
            'item.*.value_added_tax' => 'required|numeric|between:0,999999999999999.99999',
            'item.*.item_table_tax_per' => 'required|numeric|between:0,999999999999999.99999',
            'item.*.item_table_tax_fixed' => 'required|numeric|between:0,999999999999999.99999',
            'item.*.item_withholding_tax' => 'required|numeric|between:0,999999999999999.99999',
        ]);
        if($validator->fails()){
            $result = $this->getValidationErrors('error','validataionError');
            return response()->json([
                'status'   => 'error',
                'message'  => $result
            ],404);
        }

        $company = Company::where('id',session('company')['id'])->first();
        $branch = Branch::find(session('branch'));
        $invoiceId=0;


        DB::beginTransaction();

        try {
            if($validator->messages()->has('item.*.quantity')){
                $errorMsg = 'transfer_quantity_should_be_positive';
                throw new \Exception($errorMsg);
            }
            $invoice = new Invoice();
            $invoice->invoice_price_type = $request->invoice_price_type;
            $invoice->customer_id = 1;
            $invoice->company_id = $request->company_id;
            $invoice->branch_id = $request->branch_id;
            $invoice->amount_without_tax = $request->amount_without_tax;
//            $invoice->tax_percentage = ($request->tax_amount / ($request->amount_without_tax - $request->invoice_discount)) * 100 ;
            $invoice->tax_amount = $request->total_tax;
            $invoice->discount = $request->total_discount;
            $invoice->total_amount = $request->total_amount;
            $invoice->note = $request->note ?? ' ';
            $invoice->created_by = \Auth::user()->id ?? null;
            $invoice->updated_by = \Auth::user()->id ?? null;
            $invoice->save();



            $invoiceId = $invoice->id;

            foreach ($request->item as $item){



                $item_details = Item::find($item['id']);

                // Sales Invoice Items
                $invoice_item = new Invoice_item();
                $invoice_item->invoice_id = $invoice->id;
                $invoice_item->item_id = $item['id'];
                $invoice_item->item_name = $item['name'];
                $invoice_item->branch_id = $request->branch_id;
                $invoice_item->quantity = $item['quantity'];
                $invoice_item->sale_price = $item['unit_price'];
                $invoice_item->unit_price = $item_details->cost_price;
                $invoice_item->currency_rate = $item['currency_rate'];
                $invoice_item->discount_percentage = $item['discount'];
                $invoice_item->value_added_tax = $item['value_added_tax'];
                $invoice_item->item_table_tax_per = $item['item_table_tax_per'];
                $invoice_item->item_table_tax_fixed = $item['item_table_tax_fixed'];
                $invoice_item->item_withholding_tax = $item['item_withholding_tax'];
                $invoice_item->created_by = Auth::user()->id ?? null;
                $invoice_item->updated_by = Auth::user()->id ?? null;
                $invoice_item->save();

            }



            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            DB::rollback();
            $success = false;
            $result = $this->getValidationErrors('error',$e->getMessage());
            return response()->json([
                'status'   => 'error',
                'message'  => $result
            ],404);

        }

        if ($success){
            $result = $this->getValidationErrors('success',$invoiceId);
            return response()->json([
                'status'   => 'success',
                'message'  => $result,
                'invoiceId'  => $invoiceId,
            ],200);

        }else{
            Session::flash('message', "There\'s Error While Creating Your Invoice .. Please Try Again");
            Session::flash('message-type', "error");
        }

        return redirect()->back();
    }


    public function searchAutocomplete(Request $request) {
        $type = $request->type;
        $value = $request->phrase;

        // Purchasing Invoice
        if ($type == 'sales_invoice') {
            $branchId = session('branch');
            $itemRecords = [];

            // get items data
            $itemRecords = \DB::table('items as i')
                ->select('i.*')

                ->where(function ($query) use($value){
                    $query->where('i.name', 'like', '%' . $value . '%')->orWhere('i.serial_no', 'like', '%' . $value . '%');
                })
                ->select('i.id', 'i.name','i.cost_price','i.value_difference','i.discount_percentage','i.retail_price', 'i.wholesale_price')
                ->take(10)
                ->get();

            $itemRecords = array_values($itemRecords->toArray());

            // dd($medicationRecords);
            $itemRecords = array_map(function($itemRecords) {
                return array(
                    'label' => $itemRecords->name,
                    'value' => $itemRecords->id,
                    'price' => round($itemRecords->cost_price,4),
                    'retail_price' => round($itemRecords->retail_price,4),
                    'wholesale_price' => round($itemRecords->wholesale_price,4),
                    'value_difference' => round($itemRecords->value_difference,4),
                    'discount' => round($itemRecords->discount_percentage,4),
                );
            }, $itemRecords);
            return json_encode($itemRecords);
        }
    }

    public function searchItemsForSale(Request $request)
    {

        $branchId = session('branch');
        $company_id = session('company')['id'];
        $branch = Branch::find($branchId);
        $itemRecords = [];
        if($request->priceType == 0){
            $selectedFields = array('i.id', 'i.name','i.retail_price as price','i.value_difference','i.discount_percentage',\DB::raw('IF(i.id != null,null,null) as item_id'),'i.image_url','i.serial_no as code');
        }else{
            $selectedFields = array('i.id', 'i.name','i.wholesale_price as price','i.value_difference','i.discount_percentage',\DB::raw('IF(i.id != null,null,null) as item_id'),'i.image_url','i.serial_no as code');
        }
        // get items data
        $itemRecords = \DB::table('items as i')
            ->where(function ($query) use($request){
                if ($request->bar == "false"){
                    $query->where('i.name', 'like', '%' . $request->value . '%')->orWhere('i.serial_no', $request->value);
                }elseif($request->bar == "true"){

                    $query->where('i.serial_no', $request->value);
                }
            })
            ->where('i.company_id', $company_id)

            ->select($selectedFields)
            ->take(15);



        $result = $itemRecords->get();

        $itemRecords = array_values($result->toArray());
        $resultArr = [];
        $itemRecords = array_map(function($itemRecords) {
            return array(
                'label' => $itemRecords->name ?? $itemRecords->sales_kit_name,
                'value' => $itemRecords->id,
                'avail_qty' => 10000,
                'code' => $itemRecords->code,
                'price' => round($itemRecords->price,4),
                'value_difference' => round($itemRecords->value_difference,4),
                'discount' => round($itemRecords->discount_percentage,4),
                'sales_kit_name' => $itemRecords->sales_kit_name ?? null,
                'sales_kit_id' => $itemRecords->sales_kit_id ?? 0,
                'image' => $itemRecords->image_url ?? env('APP_URL').ltrim('/uploads/items/default-1.png'),
            );
        }, $itemRecords);
        return json_encode($itemRecords);
    }

    public function changeItemPriceType(Request $request)
    {
        if ($request->priceType == 0){
            $itemRecords = Item::where('id',$request->itemId)->select('id','retail_price as price')->get()->map(function ($model){
                return [
                    'id'=> $model->id,
                    'price'=> round($model->price,4),
                    'sales_kit_id' => $model->sales_kit_id,
                ];
            });
        }else{
            $itemRecords = Item::where('id',$request->itemId)->select('id','wholesale_price as price')->get()->map(function ($model){
                return [
                    'id'=> $model->id,
                    'price'=> round($model->price,4),
                ];
            });
        }

        return $itemRecords;
    }

    public function getValidationErrors($message_type , $msg)
    {
        $itemName='';
        if($message_type == 'error'){
            $explodeMsg = explode('/',$msg);

            if(count($explodeMsg) > 1){
                $ResMessage = 'validation.'.$explodeMsg[0];
                $itemName=$explodeMsg[1];
            }else{
                $ResMessage = 'validation.'.$msg;
            }
        }else{
            $ResMessage = 'validation.'.$message_type;
        }
        $result = __($ResMessage, ['Name' =>$itemName]);
        return $result;
    }

    public function getItemTaxes(Request $request)
    {
        $product = new \stdClass();
        $product->itemsDiscount = 0;
        $product->itemRate =  $request->itemRate;
        $product->valueDifference =  $request->valueDiff;
        $product->sales_total =  $request->itemAmount * ($request->itemQty * $request->itemRate);
        $product->discountAmount = ($request->itemDiscount / 100) * $product->sales_total;
        $product->net_total = $product->sales_total - $product->discountAmount;

        $data = ItemTax::where('item_id',$request->itemId)->with('tax')->get()
            ->map(function ($model)use($product){
                return [
                    'item_id'=>$model->item_id,
                    'tax_id'=>$model->tax_id,
                    'tax_ref'=>$model->tax_reference,
                    'tax_code'=>$model->tax->code,
                    'tax_name'=>$model->tax->name_ar,
                    'tax_value_calc'=>($model->tax->tax_type == 0) ? ($model->tax_value /100) * $product->net_total : ($model->tax_value + $product->net_total),
                    'tax_type'=>$model->tax->tax_type,
                    'tax_value'=>$model->tax_value,
                ];
            })->toArray();
//        $data = $data->mapToGroups(function($model, $key) {
//            return [$model['tax_ref'] => $model];
//        });

        $product->taxes = $data;
        $finalRes = $this->calculateTaxes($product);
        return response()->json($finalRes);
    }

    private function calculateTotalTaxableAmount ($product)
    {
//        $taxableItemsRef = ['T5'=>0,'T6'=>0,'T7'=>0,'T8'=>0,'T9'=>0,'T10'=>0,'T11'=>0,'T12'=>0];
//        $taxes = array_intersect_key($taxableItems,$taxableItemsRef);
//        $taxableTotalAmount = array_sum(array_map(function ($data){
//            return array_sum(array_column($data,'tax_value_calc'));
//        },$taxes)) ;
//
//        return $taxableTotalAmount;

        $totalTaxableAmount = 0;
        foreach ($product->taxes as $tax) {
            $ref = (int)substr($tax['tax_ref'], 1, strlen($tax['tax_ref']));
            if ($ref >= 5 && $ref <= 12) {
                $totalTaxableAmount += $tax['tax_value_calc'];
            }
        }
        return $totalTaxableAmount;
    }

    private function calculateTotalNonTaxableAmount ($product)
    {
//        $nonTaxableItemsRef = ['T13'=>0,'T14'=>0,'T15'=>0,'T16'=>0,'T17'=>0,'T18'=>0,'T19'=>0,'T20'=>0];
//        $taxes = array_intersect_key($nonTaxableItems,$nonTaxableItemsRef);
//        $nonTaxableTotalAmount = array_sum(array_map(function ($data){
//            return array_sum(array_column($data,'tax_value_calc'));
//        },$taxes)) ;
//
//        return $nonTaxableTotalAmount;

        $totalTaxableAmount = 0;
        foreach ($product->taxes as $tax) {
            $ref = (int)substr($tax['tax_ref'], 1, strlen($tax['tax_ref']));
            if ($ref >= 13 && $ref <= 20) {
                $totalTaxableAmount += $tax['tax_value_calc'];
            }
        }

        return $totalTaxableAmount;
    }

    public function calculateTaxes($product1)
    {
        $T1Tax = array_filter($product1->taxes, function ($item) {
            return $item['tax_ref'] == 'T1';
        });
        $T2Tax = array_filter($product1->taxes, function ($item) {
            return $item['tax_ref'] == 'T2';
        });
        $T3Tax = array_filter($product1->taxes, function ($item) {
            return $item['tax_ref'] == 'T3';
        });
        $T4Tax = array_filter($product1->taxes, function ($item) {
            return $item['tax_ref'] == 'T4';
        });

        $product1->totalTaxableAmount = $this->calculateTotalTaxableAmount($product1);
        $product1->totalNonTaxableAmount = $this->calculateTotalNonTaxableAmount($product1);


        if ($T3Tax == null) {
            $product1->FinalTaxes['T3']['name'] = 'ضريبة الجدول';
            $product1->FinalTaxes['T3']['value'] = 0;
            $product1->FinalTaxes['T3']['type'] = __('forms.inventory.sales_invoice.tax_value_fixed');
            $product1->FinalTaxes['T3']['code'] = 'Tbl02';
            $product1->FinalTaxes['T3']['percentage'] = 1;
        } else {
            $product1->FinalTaxes['T3']['name'] = 'ضريبة الجدول';
            $product1->FinalTaxes['T3']['value'] = number_format(reset($T3Tax)->tax_value,2);
            $product1->FinalTaxes['T3']['type'] = (reset($T3Tax)->tax_type != 1) ? __('forms.inventory.sales_invoice.tax_value_fixed') : __('forms.inventory.sales_invoice.tax_value_percentage');
            $product1->FinalTaxes['T3']['code'] = reset($T3Tax)->tax_code;
            $product1->FinalTaxes['T3']['percentage'] = number_format(reset($T3Tax)->tax_value,2);
        }

        if ($T2Tax == null) {
            $product1->FinalTaxes['T2']['name'] = 'ضريبة الخصم و الإضافة';
            $product1->FinalTaxes['T2']['value'] = 0;
            $product1->FinalTaxes['T2']['type'] = __('forms.inventory.sales_invoice.tax_value_percentage');
            $product1->FinalTaxes['T2']['code'] = 'Tbl01';
            $product1->FinalTaxes['T2']['percentage'] = 0;
        } else {
            $product1->FinalTaxes['T2']['name'] = 'ضريبة الخصم و الإضافة';
            $product1->FinalTaxes['T2']['value'] =  number_format($this->calculateT2($product1, reset($T2Tax))['value'],2);
            $product1->FinalTaxes['T2']['type'] =  $this->calculateT2($product1, reset($T2Tax))['type'];
            $product1->FinalTaxes['T2']['code'] =  $this->calculateT2($product1, reset($T2Tax))['code'];
            $product1->FinalTaxes['T2']['percentage'] =  '% '.number_format($this->calculateT2($product1, reset($T2Tax))['percentage']);
        }

        if ($T4Tax == null) {
            $product1->FinalTaxes['T4']['name'] = 'ضريبة المشتريات';
            $product1->FinalTaxes['T4']['value'] = 0;
            $product1->FinalTaxes['T4']['type'] = __('forms.inventory.sales_invoice.tax_value_percentage');
            $product1->FinalTaxes['T4']['code'] = 'W001';
            $product1->FinalTaxes['T4']['percentage'] = 0;
        } else {
            $product1->FinalTaxes['T4']['name'] = 'ضريبة المشتريات';
            $product1->FinalTaxes['T4']['value'] = number_format($this->calculateT4($product1, $T4Tax)['value'],2);
            $product1->FinalTaxes['T4']['type'] = $this->calculateT4($product1, $T4Tax)['type'];
            $product1->FinalTaxes['T4']['code'] = $this->calculateT4($product1, $T4Tax)['code'];
            $product1->FinalTaxes['T4']['percentage'] = '% '.number_format($this->calculateT4($product1, $T4Tax)['percentage']);
        }

        if ($T1Tax == null) {
            $product1->FinalTaxes['T1']['name'] = 'ضريبة القيمة المضافة';
            $product1->FinalTaxes['T1']['value'] = 0;
            $product1->FinalTaxes['T1']['type'] = __('forms.inventory.sales_invoice.tax_value_percentage');
            $product1->FinalTaxes['T1']['code'] = 'V001';
            $product1->FinalTaxes['T1']['percentage'] = 0;
        } else {
            $product1->FinalTaxes['T1']['name'] = 'ضريبة القيمة المضافة';
            $product1->FinalTaxes['T1']['value'] = number_format($this->calculateT1($product1, reset($T1Tax))['value']);
            $product1->FinalTaxes['T1']['type'] = $this->calculateT1($product1, reset($T1Tax))['type'];
            $product1->FinalTaxes['T1']['code'] = $this->calculateT1($product1, reset($T1Tax))['code'];
            $product1->FinalTaxes['T1']['percentage'] = '% '.number_format($this->calculateT1($product1, reset($T1Tax))['percentage']);
        }
        $product1->totalTaxes = $this->calculateTotalTaxes($product1);
        return $product1;
    }

    function calculateT2($product, $tax)
    {
        return array('value'=>($tax['tax_value'] / 100) * ($product->totalTaxableAmount + $product->net_total + $product->FinalTaxes['T3']['value'] + $product->valueDifference)
        , 'type'=>$tax['tax_type'] != 0 ? __('forms.inventory.sales_invoice.tax_value_fixed') : __('forms.inventory.sales_invoice.tax_value_percentage'),'name'=>$tax['tax_name'],'percentage'=>$tax['tax_value'],'code'=>$tax['tax_code']);
    }

    function calculateT4($product, $tax)
    {
        $tax_values = array_sum(array_column($tax,'tax_value'));
        $tax = array_values($tax);
        return array('value'=>($tax_values / 100) * ($product->net_total - $product->itemsDiscount),'type'=>$tax[0]['tax_type'] != 0 ? __('forms.inventory.sales_invoice.tax_value_fixed') : __('forms.inventory.sales_invoice.tax_value_percentage')
        ,'name'=>$tax[0]['tax_name'],'percentage'=>$tax_values,'code'=>$tax[0]['tax_code']);
    }

    function calculateT1($product, $tax)
    {
        return array('value'=>($tax['tax_value'] / 100) * ($product->totalTaxableAmount + $product->net_total + $product->valueDifference + $product->FinalTaxes['T2']['value']  + $product->FinalTaxes['T3']['value'] )
        ,'type'=>$tax['tax_type'] != 0 ? __('forms.inventory.sales_invoice.tax_value_fixed') : __('forms.inventory.sales_invoice.tax_value_percentage'),'name'=>$tax['tax_name'],'percentage'=>$tax['tax_value'],'code'=>$tax['tax_code']);
    }

    function calculateTotalTaxes($product)
    {
        return $product->FinalTaxes['T1']['value'] + $product->FinalTaxes['T2']['value'] + $product->FinalTaxes['T3']['value']  + $product->totalTaxableAmount + $product->totalNonTaxableAmount - $product->FinalTaxes['T4']['value'] - $product->itemsDiscount;
    }


    public function printInvoice(Request $request, $id){

        $invoiceIds= [];
        $ids = explode(',', $id);
        foreach($ids as $record){
            array_push($invoiceIds, urldecode(base64_decode($record)));
        }

        $company_id = session('company')['id'];
        $branch_id = session('branch');

        $sales_invoice = Invoice::with(['createdBy','invoice_items_total'])
            ->whereIn('invoices.id', $invoiceIds)
            ->where('company_id', $company_id)
            ->get();


        $general_setting = 1;

        return view('invoice.print', compact('sales_invoice','general_setting', 'invoiceIds' ));



    }
}
