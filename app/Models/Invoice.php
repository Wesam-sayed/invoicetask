<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    public function invoice_items_total()
    {
        return $this->hasMany(Invoice_item::class)
            ->select('*','item_id', \DB::raw('ROUND(sale_price, 2) as roundedUnitPrice'), \DB::raw('sum(quantity) as totalQuantity'))
            ->groupBy('item_id', 'roundedUnitPrice', 'discount_percentage');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by','id');
    }
}
