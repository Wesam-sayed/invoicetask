<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemTax extends Model
{
    protected $table = 'item_tax';

    public function tax(){
        return $this->belongsTo(Tax::class);
    }
}
