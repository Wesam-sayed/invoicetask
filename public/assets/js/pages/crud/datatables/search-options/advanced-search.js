"use strict";
var KTDatatablesSearchOptionsAdvancedSearch = function() {

	$.fn.dataTable.Api.register('column().title()', function() {
		return $(this.header()).text().trim();
	});

	var initTable1 = function() {
		// begin first table

        var table = $('#kt_datatable').DataTable({
			responsive: true,
			// Pagination settings
			dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			// read more: https://datatables.net/examples/basic_init/dom.html

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Display _MENU_',
			},

			// searchDelay: 500,
			// processing: true,
			serverSide: true,
			ajax: {
				url:'/api/getWorkOrdersDetails',
				type: 'POST',
				data: {
					// parameters for custom backend script demo
                    branch_id:branch_id,
					columnsDef: [
						 'Status', 'Type'],
				},
			},
			columns: [
				{data: 'RecordID',name:'RecordID'},
				{data: 'Item',name:'Item'},
				{data: 'Qty',name:'Qty'},
				{data: 'Branch',name:'Branch'},
				{data: 'cost_center',name:'cost_center'},
				{data: 'target_date',name:'target_date'},
				{data: 'Details',name:'Details'},
				{data: 'created_by',name:'created_by'},
				{data: 'created_at',name:'created_at'},
				{data: 'Type',name:'Type'},
                {data: 'Status',name:'Status'},
                {data: 'Actions', responsivePriority: -1},
			],

			// initComplete: function() {
			// 	this.api().columns().every(function() {
			// 		var column = this;
            //         console.log(column);
            //
            //         switch (column.title()) {
			// 			case 'Country':
			// 				column.data().unique().sort().each(function(d, j) {
			// 					$('.datatable-input[data-col-index="2"]').append('<option value="' + d + '">' + d + '</option>');
			// 				});
			// 				break;
            //
			// 			case 'Status':
			// 				var status = {
			// 					1: {'title': 'Pending', 'class': 'label-light-primary'},
			// 					2: {'title': 'Delivered', 'class': ' label-light-danger'},
			// 					// 3: {'title': 'Canceled', 'class': ' label-light-primary'},
			// 					3: {'title': 'Success', 'class': ' label-light-success'},
			// 					// 5: {'title': 'Info', 'class': ' label-light-info'},
			// 					// 6: {'title': 'Danger', 'class': ' label-light-danger'},
			// 					// 7: {'title': 'Warning', 'class': ' label-light-warning'},
			// 				};
			// 				column.data().unique().sort().each(function(d, j) {
            //                     $('.datatable-input[data-col-index="8"]').append('<option value="' + d + '">' + status[d].title + '</option>');
			// 				});
			// 				break;
            //
			// 			case 'Type':
			// 				var status = {
			// 					1: {'title': 'Online', 'state': 'danger'},
			// 					2: {'title': 'Retail', 'state': 'primary'},
			// 					3: {'title': 'Direct', 'state': 'success'},
			// 				};
			// 				column.data().unique().sort().each(function(d, j) {
            //                     $('.datatable-input[data-col-index="9"]').append('<option value="' + d + '">' + status[d].title + '</option>');
			// 				});
			// 				break;
			// 		}
			// 	});
			// },

			columnDefs: [
				{
					targets: -1,
					title: 'Actions',
					orderable: false,
					render: function(data, type, full, meta) {
					    console.log(full.RecordID)
						return '\
							<a href="'+data+'" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
								<i class="la la-edit"></i>\
							</a>\
							<a href="/manufacture/work_orders/'+btoa(full.RecordID)+'" class="btn btn-sm btn-clean btn-icon" target="_blank" title="Work Order details">\
								<i class="la la-eye"></i>\
							</a>\
							\
						';
					},
				},
                // {
                //     targets: 8,
                //     render: function(data, type, full, meta) {
                //         var status = {
                //             1: {'title': full.created_at, 'state': 'danger'},
                //             2: {'title': full.created_at, 'state': 'primary'},
                //             3: {'title': full.created_at, 'state': 'success'},
                //         };
                //         if (typeof status[data] === 'undefined') {
                //             return data;
                //         }
                //         console.log(data)
                //         return '<span class="label label-' + status[data].state + ' label-dot mr-2"></span>' +
                //             '<span class="font-weight-bold text-' + status[data].state + '">' + data + '</span>';
                //     },
                // },
                {
                    targets: 9,
                    render: function(data, type, full, meta) {
                        var status = {
                            1: {'title': full.TypeName, 'state': 'danger'},
                            2: {'title': full.TypeName, 'state': 'primary'},
                            3: {'title': full.TypeName, 'state': 'success'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }

                        return '<span class="label label-' + status[data].state + ' label-dot mr-2"></span>' +
                            '<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
                    },
                },
				{
					targets: 10,
					render: function(data, type, full, meta) {
                        var status = {
                            1: {'title': full.StatusName, 'class': 'label-light-primary'},
                            2: {'title': full.StatusName, 'class': ' label-light-danger'},
                            // 3: {'title': 'Canceled', 'class': ' label-light-primary'},
                            3: {'title': full.StatusName, 'class': ' label-light-success'},
                            // 5: {'title': 'Info', 'class': ' label-light-info'},
                            // 6: {'title': 'Danger', 'class': ' label-light-danger'},
                            // 7: {'title': 'Warning', 'class': ' label-light-warning'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}

                        return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
					},
				},

			],
		});

		var filter = function() {
			var val = $.fn.dataTable.util.escapeRegex($(this).val());
			table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
		};

		var asdasd = function(value, index) {
			var val = $.fn.dataTable.util.escapeRegex(value);
			table.column(index).search(val ? val : '', false, true);
		};

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			var params = {};
			$('.datatable-input').each(function() {
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
console.log(params);
            $.each(params, function(i, val) {
                // apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});
            table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				$(this).val('');
				table.column($(this).data('col-index')).search('', false, false);
			});
			table.table().draw();
		});

		$('#kt_datepicker').datepicker({
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

	};

	return {

		//main function to initiate the module
    // <div className="dropdown dropdown-inline">
    //     <a href="javascript:;" className="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
    //         <i className="la la-cog"></i>
    //     </a>
    //     <div className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
    //         <ul className="nav nav-hoverable flex-column">
    //             <li className="nav-item"><a className="nav-link" href="#"><i className="nav-icon la la-edit"></i><span
    //                 className="nav-text">Edit Details</span></a></li>
    //             <li className="nav-item"><a className="nav-link" href="#"><i className="nav-icon la la-leaf"></i><span
    //                 className="nav-text">Update Status</span></a></li>
    //             <li className="nav-item"><a className="nav-link" href="#"><i className="nav-icon la la-print"></i><span
    //                 className="nav-text">Print</span></a></li>
    //         </ul>
    //     </div>
    // </div>
		init: function() {
			initTable1(branch_id);
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesSearchOptionsAdvancedSearch.init();
});
