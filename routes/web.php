<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('auth');

\Auth::routes();

Route::resource('/invoice',  'HomeController');
Route::post('/searchAutocomplete', 'HomeController@searchAutocomplete')->name('searchAutocomplete');
Route::post('/searchItemsForSale', 'HomeController@searchItemsForSale')->name('searchItemsForSale');
Route::post('/changeItemPriceType', 'HomeController@changeItemPriceType')->name('changeItemPriceType');
Route::post('/invoice/item_taxes', 'HomeController@getItemTaxes')->name('getItemTaxes');
Route::get('/invoice/print/{id}', 'HomeController@printInvoice')->name('invoices.print');
//Route::get('dashboard', 'UserController@dashboard')->middleware('auth');
