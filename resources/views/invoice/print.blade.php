<html dir="rtl" lang="ar">
<head>
    <style>
        #invoice-POS{
            box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
            padding:2mm;
            margin: 0 auto;
            @if($general_setting ==1)
                width: 210mm;

            @elseif($general_setting ==2)
                width: 148mm;

            @elseif($general_setting ==3)

                width: 78mm;

            @elseif($general_setting ==4)
                width: 56mm;
            @endif

            background: #FFF;
        }

        ::selection {background: #f31544; color: #FFF;}
        ::moz-selection {background: #f31544; color: #FFF;}


        #top, #mid,#bot{ /* Targets all id with 'col-' */
            border-bottom: 1px solid #EEE;
        }

        #top{min-height: 80px;}
        #mid{
            width: 80%;
        }
        #bot{ min-height: 50px;}

        #top .logo{
        //float: left;
            height: 60px;
            width: 180px;
            background-size: 60px 60px;
        }
        .logo img{
            float: left;
            max-height: 80px;
            max-width: 180px;
        }
        .clientlogo{
            float: left;
            height: 60px;
            width: 60px;
            background-size: 60px 60px;
            border-radius: 50px;
        }

        .info1{
            width: 60%;
            height: 200px;
            float: right;
        }
        .info1 h2{
            margin-top:-5px;

        }
        .info2{
            margin-right: 70%;
            width: 55%;
            height: 200px;
        }
        .title{
            float: right;
        }
        .title p{text-align: right;}
        table{
            width: 100%;
            border-collapse: collapse;
        }
        td{
        //padding: 5px 0 5px 15px;
        //border: 1px solid #EEE
        }
        .tabletitle{
        //padding: 5px;
            font-size: .7em;
            background: #EEE;
        }

        .tablefooter{
            font-size: 0.8em;
            background: #EEE;

        }
        .service{border-bottom: 1px solid #EEE;}
        .item{width: 40mm;}
        .itemtext{font-size: 1.0em;}

        #legalcopy{
            margin-top: 5mm;
            text-align: center;
        }



        table, th, td {
            border: 1px solid black;
            text-align: center;
        }


        .footertext{
            text-align: left;
        }

        @font-face {
            font-family: TanseekModernProArabic2;
            src: url({{asset('assets/fonts/TanseekModernProArabic2.woff') }});
        }
        * {
            font-family:TanseekModernProArabic2;
        }
    </style>


</head>
<body>
<div id="invoice-POS">

    <center id="top">
        <div class="logo"><img src="{{$general_setting->company->logo ?? ''}}"/> </div>
        <div class="info">
            <h2> {{__('forms.inventory.sales_invoice.invoice')}}</h2>
        </div><!--End Info-->
    </center><!--End InvoiceTop-->

    <div id="mid">
        <div class="info1">
            <h2>{{__('forms.inventory.sales_invoice.customer_data')}}</h2>
            <p>
                {{__('forms.inventory.sales_invoice.customer')}} <b>Customer</b></br>
                {{__('forms.inventory.sales_invoice.mobile')}} <b class="translate">01128288936</b>
            </p>
        </div>
        <div class="info2">
            <h2>{{__('forms.inventory.sales_invoice.show')}}</h2>
            <p>
                {{__('forms.inventory.sales_invoice.id_no')}}: <b class="translate">
                    @foreach($invoiceIds as $invoiceId)
                        {{$invoiceId}} ,
                    @endforeach
                </b></br>
                {{__('forms.inventory.sales_invoice.date')}}: <b class="translate">{{\Carbon\Carbon::parse($sales_invoice[0]->created_at)->format('Y/m/d')}}</b></br>
                {{__('forms.inventory.sales_invoice.seller_name')}}: <b>{{ $sales_invoice[0]->createdBy->name}}</b></br>
                {{__('forms.inventory.sales_invoice.sell_time')}}: <b><span>{{ $sales_invoice[0]->created_at }}</span></b></br>

            </p>
        </div>
    </div><!--End Invoice Mid-->

    <div id="bot">

        <div id="table">
            <table>
                <tr class="tabletitle">
                    <td class="Hours"><h2>#</h2></td>
                    <td class="item"><h2>{{__('forms.inventory.sales_invoice.item_name')}}</h2></td>
                    <td class="Rate"><h2>{{__('forms.inventory.sales_invoice.unit_price')}}</h2></td>
                    <td class="Rate"><h2>{{__('forms.inventory.sales_invoice.qty')}}</h2></td>
                    <td class="Rate"><h2>{{__('forms.inventory.sales_invoice.total_amount')}}</h2></td>
                </tr>
                @php
                    $i=1;
                    $amount_without_tax=0;
                    $tax_amount=0;
                    $discount=0;
                    $total_amount=0;
                    $notes=[];
                @endphp
                @foreach($sales_invoice as $invoice)
                    @foreach($invoice->invoice_items_total as $data)
                        @php
                            $updatedUnitPrice = $data->sale_price - (($data->sale_price * $data->discount_percentage) / 100);
                            $updatedUnitPrice = $updatedUnitPrice + (($updatedUnitPrice * $data->tax_percentage) / 100);
                        @endphp
                        <tr class="service">
                            <td class="tableitem "><p class="itemtext">{{ $i }}</p></td>
                            <td class="tableitem "><p class="itemtext">{{$data->item_name}}</p></td>
                            <td class="tableitem "><p class="itemtext">{{round($updatedUnitPrice , 2)}}</p></td>
                            <td class="tableitem "><p class="itemtext">{{round($data->totalQuantity, 2)}}</p></td>
                            <td class="tableitem "><p class="itemtext">{{round($data->totalQuantity * $updatedUnitPrice, 2)}}</p></td>
                        </tr>
                        @php $i++; @endphp
                    @endforeach
                        @php
                            $amount_without_tax += $invoice->amount_without_tax;
                            $tax_amount += $invoice->tax_amount;
                            $discount += $invoice->discount;
                            $total_amount += $invoice->total_amount;
                            array_push($notes, $invoice->note);
                        @endphp
                @endforeach


<!--                <tr class="service">
                    <td class="tableitem translate footertext" colspan="3"><p class="itemtext">{{__('forms.inventory.sales_invoice.total_amount_without_tax')}}</p></td>

                    <td class="payment " colspan="2" style="font-size: 0.8em;font-weight: bold"><h4>{{round($amount_without_tax - $discount , 2)}}</h4></td>
                </tr>

-->

{{--                <tr class="service">--}}
{{--                    <td class="tableitem  footertext" colspan="3"><p class="itemtext">{{__('forms.inventory.sales_invoice.total_discounts')}}</p></td>--}}

{{--                    <td class="payment " colspan="2" style="font-size: 0.8em;font-weight: bold"><h4>-{{round($discount, 2)}}</h4></td>--}}
{{--                </tr>--}}

<!--
                <tr class="service">
                    <td class="tableitem translate footertext" colspan="3"><p class="itemtext">{{__('forms.inventory.sales_invoice.tax')}}</p></td>

                    <td class="payment " colspan="2" style="font-size: 0.8em;font-weight: bold"><h4>{{round($tax_amount, 2)}}</h4></td>
                </tr>
-->
                <tr class="tablefooter">
                    <td class="Rate" colspan="3"><h2>{{__('forms.inventory.sales_invoice.total_amount')}}</h2></td>
                    <td class="payment " colspan="2" style="font-size: 1.5em;font-weight: bold"><h4>{{round($total_amount, 2)}} £</h4></td>
                </tr>

            </table>
        </div><!--End Table-->


        <strong> {{__('forms.inventory.sales_invoice.note')}} :</strong><br/>
        <b>
            @foreach($notes as $note)
                {{$note}} <br/>
            @endforeach
        </b><br/>



        <!--  begin returned items logic -->

        <div id="legalcopy">
            <p class="legal">
                <b>
                    لقد تمت العملية بنجاح
                </b>
            </p>
        </div>

    </div><!--End InvoiceBot-->

</div><!--End Invoice-->

<script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>

<script>

	//convert numbers to arabic numbers
    var arabicNumbers = ['۰', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
    $('.translate').text(function(i, v) {
        var chars = v.split('');
        for (var i = 0; i < chars.length; i++) {
			console.log(chars[i]);
            if (/\d/.test(chars[i])) {
			    chars[i] = arabicNumbers[chars[i]];
            }
        }
        return chars.join('');
    })

    window.print();

</script>
</body>
</html>
