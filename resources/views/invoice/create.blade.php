@extends('layouts.app')
@section('headerAssets')
    {{--><style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style><script style="display: none;"></script>--}}
    {{--        <style id="savepage-cssvariables">--}}
    {{--            :root {--}}
    {{--            }--}}
    {{--        </style>--}}
    {{--        <script id="savepage-shadowloader" type="application/javascript">--}}
    {{--            "use strict";--}}
    {{--            window.addEventListener("DOMContentLoaded",--}}
    {{--                function(event) {--}}
    {{--                    savepage_ShadowLoader(5);--}}
    {{--                },false);--}}
    {{--            function savepage_ShadowLoader(c){createShadowDOMs(0,document.documentElement);function createShadowDOMs(a,b){var i;if(b.localName=="iframe"||b.localName=="frame"){if(a<c){try{if(b.contentDocument.documentElement!=null){createShadowDOMs(a+1,b.contentDocument.documentElement)}}catch(e){}}}else{if(b.children.length>=1&&b.children[0].localName=="template"&&b.children[0].hasAttribute("data-savepage-shadowroot")){b.attachShadow({mode:"open"}).appendChild(b.children[0].content);b.removeChild(b.children[0]);for(i=0;i<b.shadowRoot.children.length;i++)if(b.shadowRoot.children[i]!=null)createShadowDOMs(a,b.shadowRoot.children[i])}for(i=0;i<b.children.length;i++)if(b.children[i]!=null)createShadowDOMs(a,b.children[i])}}}--}}
    {{--        </script>--}}
    <!--begin::Page Vendors Styles(used by this page)-->
    <link href="{{asset('assets/css/pages/wizard/wizard-2.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/css/jquery-ui.css')}}" rel="Stylesheet">
    <link href="{{asset('assets/css/invoices.css')}}" rel="stylesheet" type="text/css" />
    <style>

        .custom {
            display: block;
            /*width: 100%;*/
            height: -webkit-calc(2.75rem + 2px);
            height: -moz-calc(2.75rem + 2px);
            height: calc(2.75rem + 2px);
            padding: 0.75rem 1rem;
            font-size: 1rem;
            line-height: 1.25;
            color: #4E5154;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #BABFC7;
            border-radius: 0.25rem;
            -webkit-transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            -o-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            -moz-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
        }


        @media (min-width:1201px) {

            .custom {
                width: 100%;
            }
        }

    </style>

@endsection
@section('main-content')



    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><div class="content-body">
                <div class="card">
                    <div class="card-content">
                        <div id="notify" class="alert alert-success" style="display:none;">
                            <a href="#" class="close" data-dismiss="alert">×</a>
                            <div class="message"></div>
                        </div>
                        <div class="card-body">
                            <form action="{{route('invoice.store')}}" method="post" id="data_form">
                                @csrf
                                <div class="row">
                                    <div class="col-xl-6">
                                        <label class="col-3 col-form-label">{{__('forms.inventory.sales_invoice.invoice_price_type')}}</label>
                                        <div class="col-9 col-form-label">
                                            <div class="radio-inline">
                                                <label class="radio radio-danger">
                                                    <input type="radio" class="invoice_type" id="retail"  value="0" name="invoice_price_type" checked="checked">
                                                    <span></span>{{__('forms.inventory.sales_invoice.retail_price')}}</label>
                                                <label class="radio radio-danger">
                                                    <input type="radio" class="invoice_type" id="wholesale"  value="1" name="invoice_price_type">
                                                    <span></span>{{__('forms.inventory.sales_invoice.wholesale_price')}}</label>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>{{__('forms.inventory.sales_invoice.note')}}</label>
                                            <div class="input-group input-group-solid">
                                                <textarea class="form-control form-control-solid" name="note" placeholder="{{__('forms.inventory.sales_invoice.note')}}" rows="5">{{old('note')}}</textarea>
                                            </div>
                                            <span class="text-danger">{{ $errors->has('note') ? $errors->first('note') : '' }}</span>
                                            {{--                                                    <span class="form-text text-muted">Please enter your last name.</span>--}}
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-barcode p-0"></i> 
                                                <input type="checkbox" aria-label="Enable BarCode" value="1" checked id="bar_only">
                                            </div>
                                        </div>
                                        <input type="text" class="form-control barcode" data-type="sales_invoice" id="salesInvoiceBar" name="barcode" data-qty="" data-oldCommon="" data-common="" data-name="" data-price="" data-tax="" data-discount="" data-pcode="" data-pid="" data-sales="" data-unit="" data-serial="" data-bar="" data-valueDiff=""  placeholder="{{__('forms.inventory.sales_invoice.select_barcode')}}" value="{{old('item.1.barcode')}}">
                                    </div>
                                    <i style="color:red" id="warningBarTxt"></i>
                                </div>
                                <br>
                                <div id="saman-row">
                                    <table class="table-responsive tfr my_stripe" id="purchasingTable">
                                        <thead>


                                        <tr class="item_header bg-gradient-directional-success white">
                                            {{--                                            <th width="15%" class="text-center">{{__('forms.inventory.sales_invoice.barcode')}}</th>--}}
                                            <th width="20%" class="text-center">{{__('forms.inventory.sales_invoice.item_name')}}</th>
                                            {{--                                            <th class="options" width="15%" class="text-center" hidden>{{__('forms.inventory.purchase_invoice.item_options')}}</th>--}}
                                            {{--                                            <th class="options" width="17%" class="text-center" hidden>{{__('forms.inventory.purchase_invoice.item_values')}}</th>--}}
                                            <th width="8%" class="text-center">{{__('forms.inventory.sales_invoice.qty')}}</th>
                                            <th width="10%" class="text-center">{{__('forms.inventory.sales_invoice.unit_price')}}</th>
{{--                                            <th width="5%" class="text-center mobile-hide">{{__('forms.inventory.sales_invoice.tax')}}</th>--}}
                                            <th width="7%" class="text-center mobile-hide">{{__('forms.inventory.sales_invoice.value_difference')}}</th>
{{--                                            <th width="5%" class="text-center mobile-hide">{{__('forms.inventory.sales_invoice.excise_tax_percentage')}}</th>--}}
                                            <th width="7%" class="text-center mobile-hide" >{{__('forms.inventory.sales_invoice.discount')}}</th>
                                            <th width="10%" class="text-center mobile-hide">{{__('forms.inventory.sales_invoice.currency')}}</th>
                                            <th width="7%" class="text-center">{{__('forms.inventory.sales_invoice.currency_rate')}}</th>
                                            <th width="10%" class="text-center">{{__('forms.inventory.sales_invoice.total_amount')}}</th>
                                            <th width="10%" class="text-center">{{__('forms.inventory.sales_invoice.actions')}}</th>
                                        </tr>

                                        </thead>
                                        <tbody id="tbodayData">
                                        <tr class="itemRow" id="purchaseRow_1">

                                            <td>
                                                <div class="row">
                                                    <div class="col-xl-12" id="ItemDiv_1">
                                                        <div class="form-group">
                                                            <input type="text" class="custom col-xs-12 autocomplete_txt" data-type="sales_invoice" id="salesInvoiceData_1" name="item[1][name]" placeholder="{{__('forms.inventory.sales_invoice.item')}}" value="{{old('item.1.name')}}">
                                                            <i style="color:red" id="warningTxt_1"></i>
                                                            <input type="hidden"  class="custom col-xs-12 hidden" id="salesInvoiceId_1" name="item[1][id]"  placeholder="Item ID" value="{{old('item.1.id')}}">
                                                            <input type="hidden"  class="custom col-xs-12 hidden" id="salesInvoiceSalesKitId_1" name="item[1][sales_kit_id]" placeholder="Item ID" value="{{old('item.1.sales_kit_id')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="text" class="custom col-xs-12" name="item[1][quantity]" id="salesInvoiceQty_1" data-type="qty" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,'qty')"  value="{{old('item.1.quantity',0)}}">
                                            </td>
                                            <td><input type="text" class="custom col-xs-12" name="item[1][unit_price]" id="salesInvoiceUnitPrice_1" data-type="unit_price" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,'unit_price')" autocomplete="off" value="{{old('item.1.unit_price')}}" ></td>
                                            <td class="mobile-hide"><input type="text" class="custom col-xs-12" id="salesInvoiceValueDifference_1" name="item[1][value_difference]" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,`tax`)" autocomplete="off" value="{{old('item.1.value_difference')}}" ></td>
                                            <td class="mobile-hide"><input type="text" class="custom col-xs-12" id="salesInvoiceDiscount_1" name="item[1][discount]" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,`discount`)" autocomplete="off" value="{{old('item.1.discount')}}" ></td>
                                            <td>
                                                <select class="custom col-xs-12" name="item[1][currency]" id="salesInvoiceCurrency_1">
                                                    @foreach($currencies as $record)
                                                    <option value="{{$record->id}}">{{$record->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="mobile-hide"><input type="text" class="custom col-xs-12" id="salesInvoiceCurrencyRate_1" name="item[1][currency_rate]" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,`currency_rate`)" autocomplete="off" value="{{1,old('item.1.currency_rate')}}" ></td>

                                            <td><input type="text" class="custom col-xs-12" id="salesInvoiceItemTotalAmount_1" name="item[1][itemAmount]" value="{{old('item.1.itemAmount')}}" autocomplete="off" readonly></td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" onclick="addToInvoice(this);" data-placement="top" title="{{__('forms.inventory.sales_invoice.add_to_invoice')}}" id="addToInvoice_1" hidden>
                                                    <i class="icon-plus-square"></i>{{__('forms.inventory.sales_invoice.add_to_invoice')}}</button>
                                            </td>
                                            <input type="hidden" class="custom col-xs-12" id="salesInvoiceCommonId_1" name="item[1][common_id]" value="{{old('item.1.common_id')}}" autocomplete="off">
                                            <input type="hidden" class="custom col-xs-12" id="salesInvoiceOldCommonId_1" name="item[1][old_common_id]" value="{{old('item.1.old_common_id')}}" autocomplete="off">
                                            <input type="hidden" class="custom col-xs-12" id="salesInvoiceAvailQty_1" name="item[1][avail_qty]" value="{{old('item.1.avail_qty')}}" autocomplete="off">
                                            <input type="hidden" name="item[1][item_option_value]" id="salesInvoiceItemOptionValue_1" value="{{old('item.1.item_option_value')}}">
                                            <input type="hidden" name="item[1][item_option_value_wholesale_price]" id="salesInvoiceItemOptionValueWholeSalePrice_1" value="{{old('item.1.item_option_value_wholesale_price')}}">
                                            <input type="hidden" name="item[1][item_option_value_retail_price]" id="salesInvoiceItemOptionValueRetailPrice_1" value="{{old('item.1.item_option_value_retail_price')}}">
                                            <input type="hidden" name="item[1][value_added_tax]" id="salesInvoiceValueAddedTax_1" value="{{old('item.1.value_added_tax')}}">
                                            <input type="hidden" name="item[1][item_table_tax_per]" id="salesInvoiceTableTaxPer_1" value="{{old('item.1.item_table_tax_per')}}">
                                            <input type="hidden" name="item[1][item_table_tax_fixed]" id="salesInvoiceTableTaxFixed_1" value="{{old('item.1.item_table_tax_fixed')}}">
                                            <input type="hidden" name="item[1][item_withholding_tax]" id="salesInvoiceWithholdingTax_1" value="{{old('item.1.item_withholding_tax')}}">
                                        </tr>

                                        </tbody>
                                        <tfoot>
                                        <tr class="last-item-row sub_c">
                                            <td class="add-row">
                                                <button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" onclick="addNewItemRow(); itemRowCount();" data-placement="top" title="Add product row" id="AddNewRow">
                                                    <i class="icon-plus-square"></i>Add Row</button>
                                            </td>
                                            <td colspan="7"></td>
                                        </tr>
                                        </tfoot>
                                    </table>

                                    <div class="separator separator-dashed my-5"></div>

                                    <table class="table-responsive tfr my_stripe" id="TaxTable" hidden style="width: 70%; margin: auto;margin-right: 25%;">
                                        <thead>
                                        <tr class="item_header bg-gradient-directional-success white">
                                            <th class="text-center" width="30%">{{__('forms.inventory.sales_invoice.tax_name')}}</th>
                                            <th class="text-center" width="15%">{{__('forms.inventory.sales_invoice.tax_ref')}}</th>
                                            <th class="text-center" width="15%">{{__('forms.inventory.sales_invoice.tax_code')}}</th>
                                            <th class="text-center" width="15%">{{__('forms.inventory.sales_invoice.tax_value_type')}}</th>
                                            <th class="text-center" width="15%">{{__('forms.inventory.sales_invoice.tax_percentage')}}</th>
                                            <th class="text-center" width="10%">{{__('forms.inventory.sales_invoice.tax_value')}}</th>
                                        </tr>

                                        </thead>
                                        <tbody>


                                        </tbody>
                                    </table>

                                    <div class="separator separator-dashed my-5"></div>


                                    <div class="row">
                                        <div class="col-xl-4">
                                            <label>{{__('forms.inventory.sales_invoice.item_count')}}</label>
                                            <div class="input-group input-group-solid input-group-lg">
                                                            <span class="input-group-text">
                                                                <i class="la la-shopping-cart"></i>
                                                            </span>
                                                <input type="number" class="form-control form-control-solid form-control-lg" name="itemCount" id="itemCountValue" placeholder="{{__('forms.inventory.sales_invoice.item_count')}}" readonly />
                                            </div>
                                        </div>

                                    </div>
                                    <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                        <div class="row">
                                            <!--begin::Input-->
                                            <div class="col-xl-4">
                                                <label>{{__('forms.inventory.sales_invoice.total_discount')}}</label>
                                                <div class="input-group input-group-solid input-group-lg">
                                                            <span class="input-group-text">
                                                                        <i class="la la-money"></i>
                                                            </span>
                                                    <input type="number" class="form-control form-control-solid form-control-lg" name="total_discount" id="GrandTotalDiscount" placeholder="{{__('forms.inventory.sales_invoice.total_discount')}}" onkeypress="return isNumber(event)" value="{{old('total_discount')}}" readonly />
                                                </div>
                                            </div>

                                            <div class="col-xl-4">
                                                <label>{{__('forms.inventory.sales_invoice.total_TAX')}}</label>
                                                <div class="input-group input-group-solid input-group-lg">
                                                            <span class="input-group-text">
                                                                        <i class="la la-money"></i>
                                                            </span>
                                                    <input type="number" class="form-control form-control-solid form-control-lg" name="total_tax" id="GrandTotalTax" placeholder="{{__('forms.inventory.sales_invoice.total_TAX')}}" onkeypress="return isNumber(event)" value="{{old('total_tax')}}" readonly />
                                                </div>
                                            </div>

                                            <div class="col-xl-4">
                                                <label>{{__('forms.inventory.sales_invoice.total_amount')}}</label>
                                                <div class="input-group input-group-solid input-group-lg">
                                                            <span class="input-group-text">
                                                                        <i class="la la-money"></i>
                                                            </span>
                                                    <input type="number" class="form-control form-control-solid form-control-lg" name="total_amount" id="GrandTotalPrice" placeholder="{{__('forms.inventory.sales_invoice.total_amount')}}" onkeypress="return isNumber(event)" value="{{old('total_amount')}}" readonly />
                                                </div>
                                                <span class="text-danger">{{ $errors->has('total_amount') ? $errors->first('total_amount') : '' }}</span>
                                            </div>

                                            <!--end::Input-->

                                            <!--end::Input-->

                                            <!--begin::Input-->
                                            <div class="col-xl-4" id="paymentDiv" hidden>
                                                <label>{{__('forms.inventory.sales_invoice.allocated_amount')}}</label>
                                                <div class="input-group input-group-solid input-group-lg">
                                                            <span class="input-group-text">
                                                                        <i class="la la-money"></i>
                                                            </span>
                                                    <input type="number" class="form-control form-control-solid form-control-lg" name="allocated_amount" id="allocated_amount" placeholder="{{__('forms.inventory.sales_invoice.allocated_amount')}}" onkeypress="return isNumber(event)" value="{{old('allocated_amount',0)}}" hidden/>
                                                </div>
                                                <span class="text-danger">{{ $errors->has('allocated_amount') ? $errors->first('allocated_amount') : '' }}</span>
                                            </div>
                                            <!--end::Input-->

                                            <input type="hidden" class="form-control form-control-solid form-control-lg" name="company_id" value="{{old('company_id',$company_id)}}" />
                                            <input type="hidden" class="form-control form-control-solid form-control-lg" name="branch_id"  value="{{old('branch_id',$branch_id)}}" />
                                            <input type="hidden" class="form-control form-control-solid form-control-lg" id="discount" name="invoice_discount"  value="{{old('invoice_discount')}}" />
                                            <input type="hidden" class="form-control form-control-solid form-control-lg" id="tax_amount" name="tax_amount"  value="{{old('tax_amount')}}" />
                                            <input type="hidden" class="form-control form-control-solid form-control-lg" id="amount_without_tax" name="amount_without_tax"  value="{{old('amount_without_tax')}}" />
                                        </div>

                                        {{-- Sales Kit Contents --}}
                                    </div>
                                    <div class="d-flex justify-content-between border-top mt-5 pt-10">

                                        <div>
                                            <button type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">{{__('forms.inventory.defaults.submit')}}</button>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        </div>

                    </div>
                </div>
            </div>



        </div>
    </div>
@endsection
@section('custom_js')
    <script src="{{asset('assets/js/pages/custom/wizard/wizard-2.js')}}"></script>
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <script src="{{asset('assets/js/jquery-ui.js')}}"></script>
    <script>
        $('#kt_header_top_navs .nav-item a[data-target="#kt_header_tab_7"]').addClass('active');
        $('#kt_header_navs .nav-item a[data-target="#kt_header_tab_7"]').addClass('active');
        $('#kt_header_tab_7').addClass('show active');
        $('#kt_header_tab_7 .menu-nav .menu-sales').addClass('menu-item-open menu-item-here');
        $('#kt_header_tab_7 .menu-subnav .add-sales-invoices').addClass('menu-item-active');

        itemRowCount();
        oldItems = JSON.parse('{!! json_encode(old('item')) !!}');
        function changePaymentTerms($this) {
            var value = $this.value;
            var id = $this.id;
            if (value == 2){
                $('#allocated_amount').attr('hidden',false);
                $('#paymentDiv').attr('hidden',false);
            }else{
                $('#allocated_amount').attr('hidden',true);
                $('#paymentDiv').attr('hidden',true);
            }
        }
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
        // $(".m-select2").select2();
        var Msg = '{{__('validation.confirm')}}';
        var ErrorMsg = '{{__('validation.confirmCancel')}}';
        totalAmountObj = {};
        taxAmountObj = {};
        withholdingTaxAmountObj = {};
        exciseTaxAmountObj = {};
        purchaseTaxAmountObj = {};
        amountWithoutTaxObj = {};
        amoutDiscountObj = {};

        $('select[name=customer_id]').val("{{old('customer_id')}}").trigger('change');
        $('select[name=payment_terms_id]').val("{{old('payment_terms_id')}}").trigger('change');

        $("#purchasingTable").on("click", ".deleteRowRecord", function (event) {
            rowId = $(this).data('rowid');
            totalAmountObj[rowId] =  0;
            amountWithoutTaxObj[rowId] =  0;
            taxAmountObj[rowId] =  0;
            amoutDiscountObj[rowId] =  0;

            var SumOfTotal = sum(totalAmountObj);
            var SumOfTaxAmount = sum(taxAmountObj);
            var SumOfAmountWithoutTax = sum(amountWithoutTaxObj);
            var SumOfAmountDiscount = sum(amoutDiscountObj);
            $('#GrandTotalPrice').val(SumOfTotal);
            $('#GrandTotalDiscount').val(SumOfAmountDiscount);
            $('#GrandTotalTax').val(SumOfTaxAmount);
            $('#tax_amount').val(SumOfTaxAmount);
            $('#amount_without_tax').val(SumOfAmountWithoutTax);
            $('#discount').val(SumOfAmountDiscount);
            $("#purchaseRow_"+rowId).remove();

            itemRowCount();
        });

        @if(old('item.1.item_option_value') != null)
        var CheckItem = checkItemHasOption(oldItems[1].id,1);
        $('#ItemDiv_1').removeClass().addClass('col-xl-4');
        $('#optionIdDiv_1').prop('hidden',false);
        $('#optionValueIdDiv_1').prop('hidden',false);

        $('#salesInvoiceItemOption_1').val(oldItems[1].option_id).trigger('change');
        @endif

        var x;
        var countError = '{{count($errors)}}';
        @if(!is_null(old('item')))
            oldItemsCounter = '{!! json_encode(count(old('item'))) !!}';

        for (x=0; x < oldItemsCounter -1 ;++x){
            addNewItemRow();
        }
        @endif

        function addNewItemRow() {
            lastRowId = $('#purchasingTable tbody tr:last').attr('id');
            if (typeof lastRowId == 'undefined') {
                lastRowId = "0_0";
            }
            rowIdArr = lastRowId.split("_");
            tableLength = parseInt(rowIdArr[1]);
            itemLable = '{{__('forms.inventory.sales_invoice.item')}}';
            barcodeLable = '{{__('forms.inventory.sales_invoice.select_barcode')}}';
            itemOptionLable = '{{__('forms.inventory.purchase_invoice.item_options')}}';
            itemOptionValueLable = '{{__('forms.inventory.purchase_invoice.item_options')}}';
            var addtoInvoiceBtn = '{{__('forms.inventory.sales_invoice.add_to_invoice')}}';
            // alert(tableLength);

            var options = '';
            var optionsData = JSON.parse('{!! $currencies !!}');
            var i;
            for (i = 0; i < optionsData.length; ++i) {
                options += '<option value="' + optionsData[i].id +'">' + optionsData[i].name + '</option>';
            }

            var counter = tableLength = tableLength + 1;

            // close add new row button while item not appended to invoice && hidden last tax table
            $('#TaxTable tbody').empty();
            $('#TaxTable').attr('hidden',true);
            $('#GrandTotalPrice').val('');
            $('#GrandTotalDiscount').val('');
            $('#GrandTotalTax').val('');
            if(!$('#addToInvoice_'+counter).is(":hidden")){
                $('#AddNewRow').prop('hidden',true);
            }

            var newRow = $("<tr class='=itemRow' id='purchaseRow_" + counter + "'>");
            var cols = "";
            cols +=
                '<td><div class="row"><div class="col-xl-12" id="ItemDiv_'+ counter +'"><div class="form-group">' +
                '<input type="text" class="custom col-xs-12 autocomplete_txt" data-type="sales_invoice" id="salesInvoiceData_'+ counter +'" name="item['+counter+'][name]" placeholder="'+itemLable+'">'+
                '<i style="color:red" id="warningTxt_'+ counter +'"></i><input type="hidden"  class="custom col-xs-12 hidden" id="salesInvoiceId_'+ counter +'" name="item['+ counter +'][id]" placeholder="Item ID">' +
                '<input type="hidden"  class="custom col-xs-12 hidden" id="salesInvoiceSalesKitId_'+ counter +'" name="item['+ counter +'][sales_kit_id]" placeholder="Item ID"></div></div>';

            cols += '<div class="col-xl-4" id="optionIdDiv_'+ counter +'" hidden><div class="form-group">' +
                '<select class="form-control form-control-lg" id="salesInvoiceItemOption_'+ counter +'" name="item['+ counter +'][option_id]" onchange="getItemOptionValues(this)">' +
                '<option value="">'+itemOptionLable+'</option></select></div></div>'

            cols +='<div class="col-xl-4" id="optionValueIdDiv_'+ counter +'" hidden><div class="form-group">' +
                '<select class="form-control form-control-lg" id="salesInvoiceOptionValueId_'+ counter +'" name="item['+ counter +'][option_value_id]" onchange="getItemOptionValuesData(this)">' +
                '<option value="">'+itemOptionValueLable+'</option></select></div></div></div></td>'

            cols += '<td><input type="text" class="custom col-xs-12" name="item['+ counter +'][quantity]" id="salesInvoiceQty_'+ counter +'" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,`qty`)" value="0"></td>';
            cols += '<td><input type="text" class="custom col-xs-12" name="item['+ counter +'][unit_price]" id="salesInvoiceUnitPrice_'+ counter +'" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,`unit_price`)" autocomplete="off" ></td>';
            cols += '<td class="mobile-hide"><input type="text" class="custom col-xs-12" id="salesInvoiceValueDifference_'+ counter +'" name="item['+ counter +'][value_difference]" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,`tax`)" autocomplete="off" ></td>';
            cols += '<td class="mobile-hide"><input type="text" class="custom col-xs-12" id="salesInvoiceDiscount_'+ counter +'" name="item['+ counter +'][discount]" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,`discount`)" autocomplete="off" ></td>';

            cols += '<td><select class="custom col-xs-12" name="item['+ counter +'][currency]" id="salesInvoiceCurrency_'+ counter +'">'+options+'</select></td>';
            cols += '<td class="mobile-hide"><input type="text" class="custom col-xs-12" id="salesInvoiceCurrencyRate_'+ counter +'" name="item['+ counter +'][currency_rate]" value="1" onkeypress="return isNumber(event)" onkeyup="rowTotal(this,`tax`)" autocomplete="off" ></td>';
            cols += '<td><input type="text" class="custom col-xs-12" id="salesInvoiceItemTotalAmount_'+ counter +'" name="item['+ counter +'][itemAmount]" autocomplete="off" readonly></td>';
            cols += '<td class="text-center">' +
                '<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">'+
                '<div class="btn-group mr-2">' +
                '<button type="button" data-rowid="'+ counter +'" class="btn btn-danger deleteRowRecord" title="Remove"> <i class="fa fa-minus-square"></i> </button>' +
                '</div>' +
                '<div class="input-group">' +
                '<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" onclick="addToInvoice(this);" data-placement="top" title="'+addtoInvoiceBtn+'" id="addToInvoice_'+ counter +'" hidden> <i class="icon-plus-square"></i>'+addtoInvoiceBtn+'</button>' +
                '</div>' +
                '</div>' +
                '</td> ' +
                '<input type="hidden" class="custom col-xs-12" id="salesInvoiceCommonId_'+ counter +'" name="item['+ counter +'][common_id]" autocomplete="off">' +
                '<input type="hidden" class="custom col-xs-12" id="salesInvoiceOldCommonId_'+ counter +'" name="item['+ counter +'][old_common_id]"  autocomplete="off" >'+
                '<input type="hidden" class="custom col-xs-12" id="salesInvoiceAvailQty_'+ counter +'" name="item['+ counter +'][avail_qty]"  autocomplete="off" >' +
                '<input type="hidden" name="item['+ counter +'][item_option_value]" id="salesInvoiceItemOptionValue_'+ counter +'">' +
                '<input type="hidden" name="item['+ counter +'][item_option_value_wholesale_price]" id="salesInvoiceItemOptionValueWholeSalePrice_'+ counter +'">' +
                '<input type="hidden" name="item['+ counter +'][item_option_value_retail_price]" id="salesInvoiceItemOptionValueRetailPrice_'+ counter +'">'+
                '<input type="hidden" name="item['+ counter +'][value_added_tax]" id="salesInvoiceValueAddedTax_'+ counter +'">' +
                '<input type="hidden" name="item['+ counter +'][item_table_tax_per]" id="salesInvoiceTableTaxPer_'+ counter +'">' +
                '<input type="hidden" name="item['+ counter +'][item_table_tax_fixed]" id="salesInvoiceTableTaxFixed_'+ counter +'">'+
                '<input type="hidden" name="item['+ counter +'][item_withholding_tax]" id="salesInvoiceWithholdingTax_'+ counter +'">';

            newRow.append(cols);
            $("#purchasingTable tbody").append(newRow);
            if (oldItems != null){
                $("#salesInvoiceData_" + counter).val(oldItems[counter].name).trigger('change');
                // $("#salesInvoiceBar_" + counter).val(oldItems[counter].code).trigger('change');
                $("#salesInvoiceId_" + counter).val(oldItems[counter].id).trigger('change');
                $("#salesInvoiceSalesKitId_" + counter).val(oldItems[counter].sales_kit_id).trigger('change');
                $("#salesInvoiceQty_" + counter).val(oldItems[counter].quantity).trigger('change');
                $("#salesInvoiceUnitPrice_" + counter).val(oldItems[counter].unit_price).trigger('change');
                $("#salesInvoiceValueDifference_" + counter).val(oldItems[counter].value_difference).trigger('change');
                $("#salesInvoiceDiscount_" + counter).val(oldItems[counter].discount).trigger('change');
                $("#salesInvoiceItemTotalAmount_" + counter).val(oldItems[counter].itemAmount).trigger('change');
                $("#salesInvoiceCommonId_" + counter).val(oldItems[counter].common_id).trigger('change');
                $("#salesInvoiceOldCommonId_" + counter).val(oldItems[counter].old_common_id).trigger('change');
                $("#salesInvoiceAvailQty_" + counter).val(oldItems[counter].avail_qty).trigger('change');
                $("#salesInvoiceItemOptionValueWholeSalePrice_" + counter).val(oldItems[counter].item_option_value_wholesale_price).trigger('change');
                $("#salesInvoiceItemOptionValueRetailPrice_" + counter).val(oldItems[counter].item_option_value_retail_price).trigger('change');

                $("#salesInvoiceCurrency_" + counter).val(oldItems[counter].currency).trigger('change');
                $("#salesInvoiceCurrencyRate_" + counter).val(oldItems[counter].currency_rate).trigger('change');
            }

        }

        //auto-complete script
        $(document).on('focus', '.autocomplete_txt', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            var id_arr = $(this).attr('id');
            id = id_arr.split("_");
            element_id = id[id.length - 1];
            var type = $(this).data('type');
            var priceType = $('input[name="invoice_price_type"]:checked').val();
            var warningField = 'warningTxt_';
            var textIdField = 'salesInvoiceId_';
            var selectedFieldData = 'salesInvoiceData_';
            var selectedFieldSalesKit = 'salesInvoiceSalesKitId_';
            var selectedFieldUnitPrice = 'salesInvoiceUnitPrice_';
            var selectedFieldDiscount = 'salesInvoiceDiscount_';
            var selectedFieldItemTotalAmount = 'salesInvoiceItemTotalAmount_';
            var selectedFieldQuantity = 'salesInvoiceQty_';
            var selectedFieldBarCode = 'salesInvoiceBar';
            var selectedFieldAvailQty = 'salesInvoiceAvailQty_';
            var selectedFieldValueDiff = 'salesInvoiceValueDifference_';


            //searchData = '';
            $(this).autocomplete({
                source: function (request, response) {
                    //searchData=request.term;
                    $.ajax({
                        url: '{{route('searchAutocomplete')}}',
                        dataType: "json",
                        method: 'post',
                        data: {
                            phrase: request.term,
                            type: type,
                            priceType:priceType,
                            bar:$('#bar_only').prop('checked')
                        },
                        success: function (data) {

                            var count = $.map(data, function (n, i) {
                                return i;
                            }).length;
                            if (count == 0) {
                                $("#" + warningField + element_id).html('No Records Found');
                                $("#" + textIdField + element_id).val('');
                            } else {
                                $("#" + warningField + element_id).html('');
                            }
                            //console.log(data);
                            response(data);
                        }
                    });
                },
                autoFocus: true,
                minLength: 0,
                select: function (e, ui) {
                    // remove old values
                    $('#addToInvoice_'+element_id).attr('hidden',false);
                    e.preventDefault();
                    $("#" + selectedFieldData + element_id).val('');
                    $("#" + textIdField + element_id).val('');
                    $("#" + selectedFieldSalesKit + element_id).val('');
                    $("#" + selectedFieldData + element_id).val(ui.item.label);
                    $("#" + textIdField + element_id).val(ui.item.value);
                    $("#" + selectedFieldSalesKit + element_id).val(ui.item.sales_kit_id);
                    // $("#" + selectedFieldTax + element_id).val('');
                    $("#" + selectedFieldDiscount + element_id).val('');
                    $("#" + selectedFieldValueDiff + element_id).val(ui.item.value_difference);

                    $("#" + selectedFieldDiscount + element_id).val(ui.item.discount);
                    $("#" + selectedFieldUnitPrice + element_id).val('');
                    $("#" + selectedFieldItemTotalAmount + element_id).val('');
                    $("#" + selectedFieldAvailQty + element_id).val('');
                    $("#" + selectedFieldUnitPrice + element_id).val(ui.item.retail_price);
                    $("#" + selectedFieldQuantity + element_id).val(1);
                    $("#" + selectedFieldAvailQty + element_id).val(ui.item.avail_qty);

                    var rowTotalAmount = ui.item.retail_price;
                    var totalDiscount = parseFloat(rowTotalAmount * (ui.item.discount/100)).toFixed(2);
                    // var totalTaxesAmount = parseFloat(parseFloat(totalTax) + parseFloat(totalWithholdingTax) + parseFloat(totalExciseTax) + parseFloat(totalPurchaseTax));
                    var netAmount = parseFloat((parseFloat(rowTotalAmount)) - (parseFloat(totalDiscount))).toFixed(2);
                    // amountWithoutTaxObj[element_id] =  rowTotalAmount;

                    // totalAmountObj[element_id] =  netAmount;
                    // amoutDiscountObj[element_id] =  totalDiscount;

                    // var SumOfTotalAmountDiscount = sum(amoutDiscountObj);

                    // var SumOfAmountWithoutTax = sum(amountWithoutTaxObj);
                    // var SumOfTotal = sum(totalAmountObj);

                    // $('#GrandTotalPrice').val(SumOfTotal);
                    // $('#discount').val(SumOfTotalAmountDiscount);
                    // $('#amount_without_tax').val(SumOfAmountWithoutTax);
                    $("#" + selectedFieldItemTotalAmount + element_id).val(netAmount);
                    // $("#" + selectedFieldTotalTaxAmount + element_id).val(totalTaxesAmount);
                    checkQtyIsAvail(element_id);
                    // addNewItemRow();

                }
            });
        });


        $(document).on('keyup','.barcode',function (event) {
            lastRowId = $('#purchasingTable tbody tr:last').attr('id');
            var $value = this.value;
            var id = lastRowId.split('_',2);
            var rowRecord = $('#salesInvoiceBar');
            if (!$(this).attr('readonly')) {
                // $('#v2_search_bar').keyup(function () {
                // var whr = $('#v2_warehouses option:selected').val();
                var cat = 0;
                var priceType = $('input[name="invoice_price_type"]:checked').val();

                if (this.value.length > 2) {
                    $("#SearchedItems").empty();

                    $.ajax({
                        type: "POST",
                        url:'{{route('searchItemsForSale')}}',
                        data: {"_token": "{{ csrf_token() }}",value:$value,categoryId:cat,priceType:priceType,bar:$('#bar_only').prop('checked')},
                        beforeSend: function () {
                            $("#customer-box").css("background", "#FFF url(" +  + "assets/custom/load-ring.gif) no-repeat 165px");
                        },
                        success: function (data) {

                            // $("#pos_item").html(data);
                            var result = JSON.parse(data);

                            if (result.length == 1){


                                rowRecord.data('pid',result[0].value);
                                rowRecord.data('sales',result[0].sales_kit_id);
                                rowRecord.data('bar',result[0].code);
                                rowRecord.data('name',result[0].label);
                                rowRecord.data('discount',result[0].discount);
                                rowRecord.data('price',result[0].price);
                                rowRecord.data('qty',result[0].avail_qty);
                                rowRecord.data('valueDiff',result[0].value_difference);

                                if (result[0].value == 0){
                                    $("#salesInvoiceCommonId_" + id[1]).val(result[0].sales_kit_id);
                                    // rowRecord.data('common',result[0].sales_kit_id);
                                }else{
                                    $("#salesInvoiceCommonId_" + id[1]).val(result[0].value);
                                    // rowRecord.data('common',result[0].value);
                                }

                                $("#warningBarTxt").html('');
                            }else{
                                $("#warningBarTxt").html('No Records Found');
                            }


                            // $.each(JSON.parse(data) , function (index , el) {
                            // });

                        }
                    });

                }
                // });
            }

            if (event.keyCode == 13 && !$('#salesInvoiceData_'+id[0]).attr('readonly')) {

                $('#rowRecord').attr('readonly', true);
                wait = false;
                setTimeout(function () {

                    getDataFromBar(id[1],rowRecord.data('pid'),rowRecord.data('sales'),rowRecord.data('name'),rowRecord.data('price'),rowRecord.data('tax'),rowRecord.data('discount'),rowRecord.data('bar'),$("#salesInvoiceCommonId_" + id[1]).val(),$("#salesInvoiceOldCommonId_" + id[1]).val(),rowRecord.data('qty'),rowRecord.data('valueDiff'));



                    setTimeout(function () {
                        rowRecord.attr('readonly', false);
                        rowRecord.val('');
                    },1500);
                },1000);
            }
        });

        function getDataFromBar(rowID =null, item_id=null,sales_kit_id=null ,name=null,price=null,tax=null,discount=null,barcode=null,common_id =null,old_common_id =null,avail_qty= null,valueDiff = null)
        {
            //
            if (parseInt(old_common_id) != parseInt(common_id)){

                var prevId = parseInt(rowID);
                if(rowID != 1)  prevId = parseInt(rowID) - 1;
                if (old_common_id != ''){

                    var checkRecordItem = manipulateItems(prevId,common_id);

                    if (!checkRecordItem){
                        addNewItemRow();
                        var lastRowId = $('#purchasingTable tbody tr:last').attr('id');
                        var id = lastRowId.split('_',2);
                        $("#salesInvoiceQty_" + id[1]).val(1);
                        $("#salesInvoiceData_"+id[1]).val(name);
                        // $("#salesInvoiceBar").val(barcode);
                        $("#salesInvoiceId_" + id[1]).val(item_id);
                        $("#salesInvoiceSalesKitId_" + id[1]).val(sales_kit_id);
                        $("#salesInvoiceUnitPrice_" + id[1]).val(price);
                        $("#salesInvoiceDiscount_" + id[1]).val(discount);
                        $("#salesInvoiceCommonId_" + id[1]).val(common_id);
                        $("#salesInvoiceAvailQty_" + id[1]).val(avail_qty);
                        $("#salesInvoiceValueDifference_" + id[1]).val(valueDiff);

                        rowTotal($("#salesInvoiceQty_" + id[1]),'qty');
                        $("#salesInvoiceOldCommonId_" + id[1]).val(common_id);
                        $('#salesInvoiceBar').data('oldCommon',common_id);
                    }

                }else{

                    var checkRecordItem = manipulateItems(rowID,common_id);

                    if (!checkRecordItem){

                        $("#salesInvoiceQty_" + rowID).val(1);
                        $("#salesInvoiceData_"+ rowID).val(name);
                        $("#salesInvoiceId_" + rowID).val(item_id);
                        $("#salesInvoiceSalesKitId_" + rowID).val(sales_kit_id);
                        $("#salesInvoiceUnitPrice_" + rowID).val(price);
                        $("#salesInvoiceDiscount_" + rowID).val(discount);
                        $("#salesInvoiceCommonId_" + rowID).val(common_id);
                        $("#salesInvoiceAvailQty_" + rowID).val(avail_qty);
                        $("#salesInvoiceValueDifference_" + rowID).val(valueDiff);
                        rowTotal($("#salesInvoiceQty_" + rowID),'qty');
                        $("#salesInvoiceOldCommonId_" + rowID).val(common_id);
                        $('#salesInvoiceBar').data('oldCommon',common_id);
                    }

                }

            }else{
                var oldQty = $("#salesInvoiceQty_" + rowID).val();

                if (common_id != old_common_id){
                    $("#salesInvoiceQty_" + rowID).val(1);
                    $("#salesInvoiceData_"+rowID).val(name);
                    $("#salesInvoiceId_" + rowID).val(item_id);
                    $("#salesInvoiceSalesKitId_" + rowID).val(sales_kit_id);
                    $("#salesInvoiceUnitPrice_" + rowID).val(price);
                    $("#salesInvoiceDiscount_" + rowID).val(discount);
                    $("#salesInvoiceCommonId_" + rowID).val(common_id);
                    $("#salesInvoiceAvailQty_" + rowID).val(avail_qty);
                    $("#salesInvoiceValueDifference_" + rowID).val(valueDiff);
                }else{
                    var newQty = parseInt(oldQty) + 1;
                    $("#salesInvoiceQty_" + rowID).val(newQty);


                }

                rowTotal($("#salesInvoiceQty_" + rowID),'qty');
                $("#salesInvoiceOldCommonId_" + rowID).val(common_id);
                $('#salesInvoiceBar').data('oldCommon',common_id);
            }


        }

        function manipulateItems(rowId,common_id) {
            var rowCount = parseInt($('#purchasingTable tr').length) - 2;
            // var prevId = parseInt(rowId);
            // if(rowId != 1)  prevId = parseInt(rowId) - 1;
            let itemRecordExist = false;

            for (var i = 1; i <= rowCount; i++){

                var commonExist = $('#salesInvoiceOldCommonId_'+i).val();

                if (commonExist != ''){
                    if (parseInt(commonExist) == parseInt(common_id)){
                        var oldQty = $("#salesInvoiceQty_" + i).val();
                        var newQty = parseInt(oldQty) + 1;
                        $("#salesInvoiceQty_" + i).val(newQty);
                        rowTotal($("#salesInvoiceQty_" + i),'qty');
                        itemRecordExist = true;
                        break;
                    }
                }else{
                    continue;
                }



            }
            return itemRecordExist;
        }

        $('.invoice_type').on('click',function () {
            var rowCount = parseInt($('#purchasingTable tr').length) - 2;

            var priceType = $(this).val();

            var itemObj = {}
            for (var i=1; i <= rowCount; i++){
                var itemID = $('#salesInvoiceId_'+i).val();
                var salesKitId = $('#salesInvoiceSalesKitId_'+i).val();

                loadItemsPrice(i,itemID,salesKitId,priceType);

            }
        });

        function loadItemsPrice(index, item_id, sales_kit_id , price_type) {
            $.ajax({
                type: "POST",
                url:'{{route('changeItemPriceType')}}',
                async:false,
                data: {"_token": "{{ csrf_token() }}",itemId:item_id,salesKitId:sales_kit_id,priceType:price_type },
                success: function (data) {
                    if(data.length > 0){
                        var Record = $('#purchaseRow_'+index);

                        $('#salesInvoiceUnitPrice_'+ index).val(data[0].price);
                        rowTotal($('#salesInvoiceQty_'+ index),'qty');
                    }
                    // $("#pos_item").html(data);
                }
            });


        }

        function checkQtyIsAvail(rowId) {

            var itemQty = parseInt($('#salesInvoiceQty_'+ rowId).val());
            var itemAvailQty = parseInt($('#salesInvoiceAvailQty_'+ rowId).val());
            var branchQtyConfig = '{{$branch->sell_without_stock_quantity}}';
            if (itemAvailQty != null){
                if (itemAvailQty < itemQty){
                    if (branchQtyConfig == 0){
                        alert('{{__('validation.inserted_not_enough_quantity')}}');
                        $('#salesInvoiceQty_'+ rowId).val(itemAvailQty);
                        rowTotal($('#salesInvoiceQty_'+ rowId).val(),'qty');
                    }else if(branchQtyConfig == 2){
                        alert('{{__('validation.branch_item_not_enough_quantity')}}');
                    }
                }
            }

        }

        function rowTotal($this,type) {
            var quantity = $this.value;
            var id = $this.id;

            if (quantity === undefined){
                quantity = $this[0].value;
                id = $this[0].id;
            }

            itemInput = id.split("_");
            itemInputId = parseFloat(itemInput[1]);
            checkQtyIsAvail(itemInputId);
            // show button to add item to invoice
            $('#addToInvoice_'+itemInputId).attr('hidden',false);
            var rate = $('#salesInvoiceCurrencyRate_'+itemInputId).val();
            var qty = $('#salesInvoiceQty_'+itemInputId).val();
            var rateForQty = parseFloat(qty * rate).toFixed(2);
            var unit_price = $('#salesInvoiceUnitPrice_'+ itemInputId).val();
            var discountValue = $('#salesInvoiceDiscount_'+itemInputId).val();

            var total_price = parseFloat(rateForQty * unit_price).toFixed(2); // amount Without Tax
            var totalDiscount = parseFloat(total_price * (discountValue/100)).toFixed(2);
            var netAmount = parseFloat((parseFloat(total_price)) - (parseFloat(totalDiscount))).toFixed(2);

            $('#salesInvoiceItemTotalAmount_'+ itemInputId).val(netAmount);




        }

        function addToInvoice($this) {
            var id = $this.id.split('_',2);
            var itemId = $('#salesInvoiceId_'+ id[1]).val();
            var itemDiscount = $('#salesInvoiceDiscount_'+ id[1]).val();
            var itemQty = $('#salesInvoiceQty_'+ id[1]).val();
            var itemAmount = $('#salesInvoiceUnitPrice_'+ id[1]).val();
            var itemRate = $('#salesInvoiceCurrencyRate_'+ id[1]).val();
            var valueDiff = $('#salesInvoiceValueDifference_'+ id[1]).val();

            $('#AddNewRow').prop('hidden',false);
            if($('#'+$this.id).is(":hidden")){
                $('#AddNewRow').prop('hidden',true);
            }

            $.ajax({
                'url':'{{route('getItemTaxes')}}',
                'type':'POST',
                'data':{
                    '_token':'{{csrf_token()}}',itemId:itemId,itemRate:itemRate,itemAmount:itemAmount,itemDiscount:itemDiscount,itemQty:itemQty,valueDiff:valueDiff
                },
                success : function (data) {
                    var table = $('#TaxTable');
                    var tableBody = $('#TaxTable tbody');
                    if (data){
                        $('#'+$this.id).attr('hidden',true);
                        table.attr('hidden',false);
                        tableBody.empty();
console.log(data)
                        $.each(data.FinalTaxes ,function (key,val) {
                            tableBody.append($('<tr>' +
                                '<td id="tax_name_'+id[1]+'">'+val.name+'</td>' +
                                '<td id="tax_ref_'+id[1]+'">'+key+'</td>' +
                                '<td id="tax_code_'+id[1]+'">'+val.code+'</td>' +
                                '<td id="tax_value_type_'+id[1]+'">'+val.type+'</td>' +
                                '<td id="tax_percentage_'+id[1]+'">'+val.percentage+'</td>' +
                                '<td id="tax_value_'+id[1]+'">'+val.value+'</td>'+
                                '</tr>'));
                        });

                        $('#salesInvoiceValueAddedTax_'+id[1]).val(data.FinalTaxes['T1'].value);
                        $('#salesInvoiceTableTaxPer_'+id[1]).val(data.FinalTaxes['T2'].value);
                        $('#salesInvoiceTableTaxFixed_'+id[1]).val(data.FinalTaxes['T3'].value);
                        $('#salesInvoiceWithholdingTax_'+id[1]).val(data.FinalTaxes['T4'].value);

                        // bind summation of taxes calcs
                        amountWithoutTaxObj[id[1]] =  data.sales_total;
                        amoutDiscountObj[id[1]] =  data.discountAmount;
                        totalAmountObj[id[1]] =  parseFloat(data.net_total) + parseFloat(data.totalTaxes);
                        taxAmountObj[id[1]] =  data.totalTaxes;

                        $('#discount').val(sum(amoutDiscountObj));
                        $('#amount_without_tax').val(sum(amountWithoutTaxObj));
                        $('#GrandTotalDiscount').val(sum(amoutDiscountObj));
                        $('#GrandTotalTax').val(sum(taxAmountObj));
                        $('#GrandTotalPrice').val(sum(totalAmountObj));

                    }
                }
            })
        }

        function sum( obj ) {
            var sum = 0;
            for( var el in obj ) {
                if( obj.hasOwnProperty( el ) ) {
                    sum += parseFloat( obj[el] );
                }
            }
            return sum.toFixed(2);
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        if (localStorage.bar_only && localStorage.bar_only != '') {
            $('#bar_only').attr('checked', 'checked');
            $('#AddNewRow').prop('hidden',true);
        } else {
            $('#bar_only').removeAttr('checked');
            $('#AddNewRow').prop('hidden',false);
        }

        $('#bar_only').click(function () {
            var rowCount = parseInt($('#purchasingTable tr').length) - 2;

            if ($('#bar_only').is(':checked')) {

                localStorage.bar_only = $('#bar_only').val();
                $('#AddNewRow').prop('hidden',true);
                for (var i=1; i <= rowCount; i++){
                    var SearchInput = $('#salesInvoiceData_'+i);
                    $(SearchInput).attr('readonly', true);

                }
            } else {
                localStorage.bar_only = '';
                $('#AddNewRow').prop('hidden',false);
                for (var i=1; i <= rowCount; i++){
                    var SearchInput = $('#salesInvoiceData_'+i);
                    $(SearchInput).attr('readonly', false);

                }
            }

            // $('#v2_search_bar').attr('readonly', false);
        });

        function itemRowCount(){
            var rowCount = $('#tbodayData tr').length;
            if (rowCount == 1){
                $('#AddNewRow').prop('hidden',false);
            }
            $('#itemCountValue').val(rowCount);
        }
        function checkQtyIsAvail(rowId) {

            var itemQty = parseInt($('#salesInvoiceQty_'+ rowId).val());
            var itemAvailQty = parseInt($('#salesInvoiceAvailQty_'+ rowId).val());
            var branchQtyConfig = '{{$branch->sell_without_stock_quantity}}';
            if (itemAvailQty != null){
                if (itemAvailQty < itemQty){
                    if (branchQtyConfig == 0){
                        alert('{{__('validation.inserted_not_enough_quantity')}}');
                        $('#salesInvoiceQty_'+ rowId).val(itemAvailQty);
                        rowTotal($('#salesInvoiceQty_'+ rowId).val(),'qty');
                    }else if(branchQtyConfig == 2){
                        alert('{{__('validation.branch_item_not_enough_quantity',['item'=>'${item_name}'])}}');
                    }
                }
            }

        }

        function hideOnMobile(x) {
            if (x.matches) { // If media query matches
                $(".mobile-hide").hide();
            } else {
                $(".mobile-hide").show();
            }
        }

        var pageWidth = window.matchMedia("(max-width: 800px)")
        hideOnMobile(pageWidth) // Call listener function at run time
        pageWidth.addListener(hideOnMobile) // Attach listener function on state changes



        $('#data_form').on('submit',function (e) {
            e.preventDefault();
            var data = $(this).serialize();
            $.ajax({
                url:'{{route('invoice.store')}}',
                type:'POST',
                data:data,
                success:function (data) {
                    if (data.status == 'success'){
                        var app_url = '{{ env('APP_URL') }}';
                        var url = app_url+'/invoice/print/'+btoa(data.invoiceId)

                        var win = window.open(url, '_blank');
                        if (win) {
                            //Browser has allowed it to be opened
                            win.focus();
                            location.reload();
                        } else {
                            //Browser has blocked it
                            alert('Please allow popups for this website');
                        }
                    }


                },
                error:function (data) {
                    Swal.fire({
                        position: "top-right",
                        icon: data.responseJSON.status,
                        title: data.responseJSON.message,
                        showConfirmButton: false,
                        timer: 4500
                    });


                }
            });
        });

    </script>
@endsection


