<!--begin::Head-->
<head><base href="">
    <meta charset="utf-8" />
    <title>Invoice</title>
    <meta name="description" content="Updates and statistics" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

@yield('headerAssets')

<!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    @if (App::isLocale('ar'))
        <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles-->
        <!--begin::Global Theme Styles(used by all pages)-->
        <link href="{{asset('assets/plugins/global/plugins.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/style.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
        {{--    <link href="assets/css/themes/layout/header/base/light.rtl.css" rel="stylesheet" type="text/css" />--}}
        {{--    <link href="assets/css/themes/layout/header/menu/light.rtl.css" rel="stylesheet" type="text/css" />--}}
        {{--    <link href="assets/css/themes/layout/brand/dark.rtl.css" rel="stylesheet" type="text/css" />--}}
        {{--    <link href="assets/css/themes/layout/aside/dark.rtl.css" rel="stylesheet" type="text/css" />--}}
        <style>


            @font-face {
                font-family: Almarai;
                src: url(/assets/fonts/Almarai-Regular.ttf);
            }
            * :not(i) {
                font-family:Almarai, sans-serif !important;
            }

            body{
                width: 90%
            }
            @media (max-width:1450px) {

                #app_logo{
                    display:none;
                }
            }

        </style>

@else
    <!--begin::Page Vendors Styles -->
        <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
        <!--begin::Base Styles -->
        <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
@endif
<!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <!--end::Layout Themes-->
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
</head>
