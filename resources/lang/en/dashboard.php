<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Login'=>[
        'email' => 'البريد الالكتروني',
        'password' => 'كلمة السر',
    ],

    'sales_invoice'=>[
        'title'=>'Sales Invoices Status',
        'daily_sales'=>'Daily Sales',
        'weekly_sales'=>'Weekly Sales',
        'monthly_sales'=>'Monthly Sales',
        'yearly_sales'=>'Yearly Sales',
    ],
    'sales_target'=>[
        'title'=>'Sales Target Status for ( :Name )',
        'branch_target'=>'Branch Sales Target For this Month',
        'total_sales_amount'=>'Branch Total Sales Till Now',
    ],

    'trends'=>[
        'latest_items'=>'Trending Items',
    ],

];
