<?php

use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('items')->truncate();

        $dataArr = array(
            [
                'name'=>'المنتج-1',
                'company_id'=>1,
                'serial_no'=>'0001',
                'sku'=>'prod-01',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'value_difference'=>10,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'المنتج الاول',
                'active'=>1,
            ],
            [
                'name'=>'المنتج-2',
                'company_id'=>1,
                'serial_no'=>'0002',
                'sku'=>'prod-02',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'value_difference'=>10,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'المنتج الثاني',
                'active'=>1,
            ],
            [
                'name'=>'المنتج-3',
                'company_id'=>1,
                'serial_no'=>'0003',
                'sku'=>'prod-03',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'value_difference'=>10,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'المنتج الثالث',
                'active'=>1,
            ],
            [
                'name'=>'المنتج-4',
                'company_id'=>1,
                'serial_no'=>'0004',
                'sku'=>'prod-04',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'value_difference'=>10,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'المنتج الرابع',
                'active'=>1,
            ],
            [
                'name'=>'المنتج-5',
                'company_id'=>1,
                'serial_no'=>'0005',
                'sku'=>'prod-05',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'value_difference'=>10,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'المنتج الخامس',
                'active'=>1,
            ],
        );

        foreach ($dataArr as $record){
            \App\Models\Item::create($record);
        }
    }
}
