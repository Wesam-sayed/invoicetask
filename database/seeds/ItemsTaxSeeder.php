<?php

use Illuminate\Database\Seeder;

class ItemsTaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('item_tax')->truncate();

        $dataArr = array(
            [
                'item_id'=>1,
                'tax_id'=>1,
                'tax_reference'=>'T1',
                'tax_value'=>5,
            ],
            [
                'item_id'=>1,
                'tax_id'=>11,
                'tax_reference'=>'T2',
                'tax_value'=>14,
            ],
            [
                'item_id'=>1,
                'tax_id'=>30,
                'tax_reference'=>'T6',
                'tax_value'=>50,
            ],
            [
                'item_id'=>5,
                'tax_id'=>37,
                'tax_reference'=>'T10',
                'tax_value'=>12,
            ],
            [
                'item_id'=>5,
                'tax_id'=>56,
                'tax_reference'=>'T20',
                'tax_value'=>200,
            ],
        );

        foreach ($dataArr as $record){
            \App\Models\ItemTax::create($record);
        }
    }
}
