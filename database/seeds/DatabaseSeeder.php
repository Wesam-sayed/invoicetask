<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AdminUserSeeder::class);
         $this->call(CompanySeeder::class);
         $this->call(BranchSeeder::class);
         $this->call(ItemSeeder::class);
         $this->call(TaxesSeeder::class);
         $this->call(ItemsTaxSeeder::class);
         $this->call(CurrencySeeder::class);
    }
}
