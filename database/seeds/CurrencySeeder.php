<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('currencies')->truncate();

        $dataArr = array(
            [
                'name'=>'EGP',
                'symbol'=>'£',
                'symbol_position'=>'l',
                'active'=>1,
            ],
            [
                'name'=>'Dollar',
                'symbol'=>'$',
                'symbol_position'=>'l',
                'active'=>1,
            ],
        );

        foreach ($dataArr as $record){
            \App\Models\Currency::create($record);
        }
    }
}
