<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemTaxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_item_tax', function (Blueprint $table) {
            $table->id();
            $table->integer('invoice_item_id');
            $table->integer('item_id');
            $table->integer('tax_id');
            $table->string('tax_reference');
            $table->decimal('value', 20, 5)->default(0);
            $table->timestamps();

            $table->index('invoice_item_id');
            $table->index('item_id');
            $table->index('tax_id');
            $table->index('tax_reference');
            $table->index('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_item_tax');
    }
}
