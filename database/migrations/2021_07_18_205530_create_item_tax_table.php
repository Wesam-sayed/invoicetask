<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemTaxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_tax', function (Blueprint $table) {
            $table->id();
            $table->integer('item_id');
            $table->integer('tax_id');
            $table->string('tax_reference');
            $table->decimal('tax_value', 20, 5)->default(0);
            $table->timestamps();

            $table->index('item_id');
            $table->index('tax_id');
            $table->index('tax_reference');
            $table->index('tax_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_tax');
    }
}
