<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('company_id');
            $table->string('serial_no');
            $table->string('sku')->nullable();
            $table->decimal('cost_price', 20, 5);
            $table->decimal('retail_price', 20, 5);
            $table->decimal('wholesale_price', 20, 5);
            $table->decimal('value_difference', 20, 5);
//            $table->decimal('withholding_tax_percentage', 6, 4);
//            $table->decimal('excise_tax_percentage', 6, 4);
//            $table->decimal('purchase_tax_percentage', 6, 4);
            $table->decimal('discount_percentage', 6, 4);
            $table->decimal('quantity', 20, 5);
            $table->string('image_url');
            $table->decimal('reorder_level', 20, 5)->default(0);
            $table->string('description')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->timestamps();

            $table->index('company_id');
            $table->index('serial_no');
            $table->index('name');
            $table->index('sku');
            $table->index('cost_price');
            $table->index('retail_price');
            $table->index('wholesale_price');
//            $table->index('tax_percentage');
            $table->index('discount_percentage');
            $table->index('quantity');
            $table->index('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
